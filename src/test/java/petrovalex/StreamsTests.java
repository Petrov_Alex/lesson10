package petrovalex;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StreamsTests {
  private final ByteArrayOutputStream out = new ByteArrayOutputStream();

  private Streams<String> stringStreams = new Streams<>();
  private Collection<String> collection = Arrays.asList("lorem", "ipsum", "dolor", "sit",
    "amet", "consectetur", "adipiscing", "elit");
  private Predicate<String> predicate;

  @BeforeEach
  private void catchOutput() {
    System.setOut(new PrintStream(out));
  }

  @AfterEach
  private void resetOutput() {
    out.reset();
  }

  @Test
  public void filterCollectionTest() {
    predicate = s -> s.startsWith("a");
    Collection expected = Arrays.asList("amet", "adipiscing");
    Collection actual = stringStreams.filterCollection(collection, predicate);
    assertEquals(expected, actual);

    predicate = s -> s.length() <= 4;
    expected = Arrays.asList("sit", "amet", "elit");
    actual = stringStreams.filterCollection(collection, predicate);

    assertEquals(expected, actual);
  }

  @Test
  public void generateWithZeroRemainderTest1() {
    final int divider = 27;
    Collection expected = Arrays.asList(27, 54, 81, 108, 135, 162, 189, 216, 243, 270);
    Collection actual = stringStreams.generateWithZeroRemainder(divider);

    assertEquals(expected, actual);

    // Распечатаем
    actual.forEach(System.out::println);
    String output = out.toString();
    // Поместим в коллекцию
    Collection actualStrings = Arrays .asList(output.split("[\r]\n"));

    assertEquals(10, actualStrings.size());

    Collection expectedStrings = Arrays.asList("27", "54", "81", "108", "135", "162", "189", "216", "243", "270");
    assertEquals(expectedStrings, actualStrings);
  }

  @Test
  public void printSortedArrayTest() {
    List expected = Arrays.asList("sit", "amet", "elit", "dolor", "ipsum", "lorem",
      "adipiscing", "consectetur");

    String[] inputArray = collection.stream().toArray(String[]::new);
    stringStreams.printSortedArray(inputArray);

    String[] actual = getOutputArray();

    assertEquals(expected, Arrays.asList(actual));
  }

  @Test
  public void createLengthCountMapTest() {
    Map<Integer, Integer> map = Map.of(
      3, 1,
      4, 2,
      5, 3,
      10, 1,
      11, 1);
    TreeMap<Integer, Integer> expected = new TreeMap<>(map);

    String[] inputArray = collection.stream().toArray(String[]::new);
    Map<Integer, Long> actual = stringStreams.createLengthCountMap(inputArray);

    assertEquals(expected.size(), actual.size());
    assertEquals(expected.keySet(), actual.keySet());
    assertEquals(expected.toString(), actual.toString());
  }

  private String[] getOutputArray() {
    String output = out.toString();
    return output.split("[\r]\n");
  }
}
