package petrovalex;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams<T> {

  public Collection<T> filterCollection(Collection<T> collection, Predicate<T> predicate) {
    return collection.stream()
      .filter(predicate)
      .collect(Collectors.toCollection(ArrayList::new));
  }

  public Collection<Integer> generateWithZeroRemainder(int divider) {
    Predicate<Integer> predicate = (i -> i % divider == 0);
    return Stream.iterate(1, x -> x + 1)
      .filter(predicate)
      .limit(10)
      .collect(Collectors.toCollection(ArrayList::new));
  }

  public void printSortedArray(String[] array) {
    Comparator<String> comparator = (s1, s2) -> {
      if (s1.length() < s2.length())
        return -1;
      if (s1.length() > s2.length())
        return 1;
      return s1.compareTo(s2);
    };

    Arrays.stream(array)
      .sorted(comparator)
      .forEach(System.out::println);
  }

  public Map<Integer, Long> createLengthCountMap(String[] array) {
    return Arrays.stream(array).
      collect(Collectors.groupingBy(
        String::length, Collectors.counting()));
  }
}
